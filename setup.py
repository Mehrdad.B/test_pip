from setuptools import setup

setup(name='testing pip',
      version='0.2',
      description='testing private pip',
      url='https://gitlab.com/Mehrdad.B/test_pip/',
      license='MIT',
      packages=['test'])
